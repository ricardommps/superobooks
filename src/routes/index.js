import BooksList from '../BooksList/BooksListPage'

const routes = [
    {
        path:'/',
        component: BooksList
    }
]

export default routes;