import React, { Component } from 'react';
import './App.css';

import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import store from './store';
import routes from './routes';

// Render do App

const basename = process.env.PUBLIC_URL ? process.env.PUBLIC_URL : '/';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <BrowserRouter basename={basename}>
            <div>
              <Switch>
                {routes.map(route => (
                  <Route
                    key={route.path}
                    path={route.path}
                    component={route.component}
                  />
                ))}
              </Switch>
            </div>
          </BrowserRouter>
        </div>
      </Provider>
    );
  }
}

export default App;
