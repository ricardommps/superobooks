import React, { Component } from 'react';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { fade } from '@material-ui/core/styles/colorManipulator';
import TextField from '@material-ui/core/TextField';
import CalendarToday from '@material-ui/icons/CalendarToday';

import BookDetails from './BookDetails';


const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#ccc',
    color: theme.palette.common.black,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  table: {
    minWidth: 700,
  },
  textField: {
    width: 55,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 2,
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchInput: {
    border: '1px solid #e0e0e0'
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '100%',
      '&:focus': {
        width: '100%',
      },
    },
  },
  paper: {
    padding: theme.spacing.unit * 2,
    display: 'flex',
    flexWrap: 'wrap',
  },
  icon: {
    margin: '20px 16px 0 0',
    color: theme.palette.action.active,
  },
  emptyResult:{
    width: '100%',
    display: 'table',
    fontFamily: "Roboto",
    borderSpacing: 0,
    borderCollapse: 'collapse'
  },
  filterButton:{
    bottom: '5px'
  },
  clearButton:{
    bottom: '5px',
    marginLeft: '15px'
  }

});

class BookTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchInput: '',
      startYear: '',
      endYear: '',
      books: [],
      book: {},
      page: 0,
      rowsPerPage: 5,
      openDetails: false
    }
    this.handleFilterSelected = this.handleFilterSelected.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleYearChange = this.handleYearChange.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.handleClearFilter = this.handleClearFilter.bind(this);
    this._searchBooks = this._searchBooks.bind(this);
    this._filterBooks = this._filterBooks.bind(this);
  }

  componentDidMount() {
    if (this.props.books)
      this.setState({ books: this.props.books })
  }

  handleClearFilter = () => {
    this.setState({
      startYear:'',
      endYear:'',
      books: this._searchBooks()
    })
  }

  _searchBooks = () => {
    const {books} = this.props;
    const { searchInput } = this.state;
    if(searchInput.length > 0){
      let resultFilter = books.filter(el => {
        return el.title.toLowerCase().includes(searchInput.toLowerCase()) ||
          el.author.toLowerCase().includes(searchInput.toLowerCase()) ||
          el.isbn.toLowerCase().includes(searchInput.toLowerCase())
      })
      return resultFilter
    }else{
      return books
    }
  }

  _filterBooks = () => {
    const {books} = this.props;
    if(this.state.startYear.length == 4 && this.state.endYear.length == 4){
      let resultFilter = books.filter(el => {
        return parseInt(el.published, 10) >= parseInt(this.state.startYear, 10) &&  
          parseInt(el.published, 10) <= parseInt(this.state.endYear, 10)
      })
      return resultFilter
    }else{
      return books
    }
  }

  handleFilter = () => {
    let books = this._searchBooks();
    let resultFilter = books.filter(el => {
      return parseInt(el.published, 10) >= parseInt(this.state.startYear, 10) &&  
        parseInt(el.published, 10) <= parseInt(this.state.endYear, 10)
    })
    this.setState({ books: resultFilter })
  }
  
  handleYearChange = (input, value) => {
    this.setState({
      [input]: value
    })
  }
  handleCloseModal = () => {
    this.setState({
      openDetails: false,
      book: {}
    })
  }

  handleDetails = (book) => {
    this.setState({
      openDetails: true,
      book: book
    })
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleFilterSelected = () => {
    let books = this._filterBooks();
    const { searchInput } = this.state;
    let resultFilter = books.filter(el => {
      return el.title.toLowerCase().includes(searchInput.toLowerCase()) ||
        el.author.toLowerCase().includes(searchInput.toLowerCase()) ||
        el.isbn.toLowerCase().includes(searchInput.toLowerCase())
    })
    this.setState({ books: resultFilter })
  }
  render() {
    const { classes } = this.props;
    const { rowsPerPage, page, books } = this.state;
    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              Supero
            </Typography>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <div className={classes.searchInput}>
                <InputBase
                  placeholder="Busque livros por título, autor ou ISBN"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  onChange={e => this.setState({ searchInput: e.target.value })}
                />
              </div>
            </div>
            <Button
              variant="contained"
              className={classNames(classes.margin, classes.cssRoot)}
              onClick={() => this.handleFilterSelected()}
            >
              BUSCAR
            </Button>
          </Toolbar>
        </AppBar>
        <Paper className={classes.paper}>
          <div style={{ position: 'relative', bottom: '10px' }}>
            <Typography inline style={{ paddingRight: '15px' }}>
              Filtrar ano de publicação:
                </Typography>
            <TextField
              id="startYear"
              name="startYear"
              className={classes.textField}
              margin="normal"
              type="number"
              value={this.state.startYear}
              onChange={e => this.handleYearChange('startYear', e.target.value)}
            />
            <CalendarToday className={classes.icon} />
            <Typography inline style={{ paddingRight: '20px' }}>ate</Typography>
            <TextField
              id="endYear"
              name="endYear"
              className={classes.textField}
              margin="normal"
              type="number"
              value={this.state.endYear}
              onChange={e => this.handleYearChange('endYear', e.target.value)}
            />
            <CalendarToday className={classes.icon} />
            {(this.state.startYear.length == 4 && this.state.endYear.length == 4 )&&
              <div style={{ display: 'inline-block' }}>
                <Button 
                  variant="outlined" 
                  size="small" 
                  color="default" 
                  className={classes.filterButton}
                  onClick={() => this.handleFilter()}>
                  Filtrar
                </Button>
                <Button 
                  variant="outlined" 
                  size="small" 
                  color="default" 
                  className={classes.clearButton}
                  onClick={() => this.handleClearFilter()}>
                  Limpar filtros
                </Button>
              </div>
            }
            
          </div>
          {books.length > 0 ?
            <div>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <CustomTableCell>Livro</CustomTableCell>
                    <CustomTableCell>ISBN</CustomTableCell>
                    <CustomTableCell>Autor</CustomTableCell>
                    <CustomTableCell>Editora</CustomTableCell>
                    <CustomTableCell align="right">Ano</CustomTableCell>
                    <CustomTableCell>Ações</CustomTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {books
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(book => (
                      <TableRow key={book.id}>
                        <CustomTableCell component="th" scope="row">
                          {book.title}
                        </CustomTableCell>
                        <CustomTableCell>{book.isbn}</CustomTableCell>
                        <CustomTableCell component="th" scope="row">
                          {book.author}
                        </CustomTableCell>
                        <CustomTableCell component="th" scope="row">
                          {book.publisher}
                        </CustomTableCell>
                        <CustomTableCell align="right">{book.published}</CustomTableCell>
                        <CustomTableCell component="th" scope="row">
                          <Button color="primary" className={classes.button} onClick={() => this.handleDetails(book)}>
                            Detalhes
                          </Button>
                        </CustomTableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={books.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </div>
            :
            <div className={classes.emptyResult}>Não foram encontrados resultado para sua pequisa</div>
          }

        </Paper>
        <BookDetails
          handleClose={this.handleCloseModal}
          openDetails={this.state.openDetails}
          book={this.state.book}
        />
      </div>
    )
  }
}

export default withStyles(styles)(BookTable);