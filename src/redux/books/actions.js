import types from '../../constants/actionsTypes.constants';

export const selectBook = book => {
   
    return {
      type: types.BOOK_SELECTED,
      payload: book
    };
  };
  
