import React, { Component } from 'react';
import { connect } from 'react-redux';
import BookTable from './Book_Table';
class BooksListPage extends Component {
    render(){
        return (
            <React.Fragment>
                <BookTable books={this.props.books} />
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    // Whatever is returned will show up
    // as props inside of BookList.
    return {
      books: state.books
    };
  }
  
  export default connect(mapStateToProps)(BooksListPage);