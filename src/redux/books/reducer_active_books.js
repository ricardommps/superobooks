import types from '../../constants/actionsTypes.constants';

function activeBookReducer(state = null, action) {
    switch (action.type) {
        case types.BOOK_SELECTED:
            return action.payload;
        default:
            return state;
    }
}

export default activeBookReducer;