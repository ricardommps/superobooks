import { combineReducers } from 'redux';
import booksReducer from './books/reducer_books';
import activeBookReducer from './books/reducer_active_books';
import ui from './ui/reducer';

export default combineReducers({
    books: booksReducer,
    activeBook: activeBookReducer,
    ui
});