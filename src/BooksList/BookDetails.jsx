import React, { Component } from 'react';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

class BookDetails extends Component {
    render() {
        const { book, handleClose, openDetails } = this.props;
        return (
            <Dialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={openDetails}
                maxWidth="sm"
                fullWidth="true"
            >
                <DialogTitle id="simple-dialog-title">Informações sobre o livro</DialogTitle>
                <DialogContent>
                    <Typography gutterBottom variant="subtitle1">
                        Título: {book.title}
                    </Typography>
                    <Typography gutterBottom>
                        ISBN: {book.isbn}
                    </Typography>
                    <Typography gutterBottom>
                        Autor: {book.author}
                    </Typography>
                    <Typography gutterBottom>
                        Editora: {book.publisher}
                    </Typography>
                    <Typography gutterBottom>
                        Ano: {book.published}
                    </Typography>
                    <Typography gutterBottom>
                        Idioma: {book.language}
                    </Typography>
                    <Typography gutterBottom>
                        Peso: {book.weight}
                    </Typography>
                    <Typography gutterBottom>
                        Dimensão: {book.admeasurement}
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Fechar
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default BookDetails