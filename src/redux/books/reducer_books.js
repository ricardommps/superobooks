function booksReducer() {
    return [
      {
        "id":1,
        "isbn": "9781593275846",
        "title": "Eloquent JavaScript, Second Edition",
        "subtitle": "A Modern Introduction to Programming",
        "author": "Marijn Haverbeke",
        "published": "2014",
        "publisher": "No Starch Press",
        "language": "English",
        "admeasurement":"15,88 × 23,50 cm",
        "weight":"235,068 g",
        "pages": 472
    },
    {
        "id":2,
        "isbn": "9781449331818",
        "title": "Learning JavaScript Design Patterns",
        "subtitle": "A JavaScript and jQuery Developer's Guide",
        "author": "Addy Osmani",
        "published": "2012",
        "publisher": "O'Reilly Media",
        "language": "English",
        "admeasurement":"15,88 × 23,50 cm",
        "weight":"235,068 g",
        "pages": 254
    },
    {
        "id":3,
        "isbn": "9781449365035",
        "title": "Speaking JavaScript",
        "subtitle": "An In-Depth Guide for Programmers",
        "author": "Axel Rauschmayer",
        "published": "2014",
        "publisher": "O'Reilly Media",
        "language": "English",
        "admeasurement":"15,88 × 23,50 cm",
        "weight":"235,068 g",
        "pages": 460
    },
    {
        "id":4,
        "isbn": "9781491950296",
        "title": "Programming JavaScript Applications",
        "subtitle": "Robust Web Architecture with Node, HTML5, and Modern JS Libraries",
        "author": "Eric Elliott",
        "published": "2014",
        "publisher": "O'Reilly Media",
        "language": "English",
        "admeasurement":"15,88 × 23,50 cm",
        "weight":"235,068 g",
        "pages": 254,
        "description": "Take advantage of JavaScript's power to build robust web-scale or enterprise applications that are easy to extend and maintain. By applying the design patterns outlined in this practical book, experienced JavaScript developers will learn how to write flexible and resilient code that's easier-yes, easier-to work with as your code base grows.",
        "website": "http://chimera.labs.oreilly.com/books/1234000000262/index.html"
    },
    {
        "id":5,
        "isbn": "9781593277574",
        "title": "Understanding ECMAScript 6",
        "subtitle": "The Definitive Guide for JavaScript Developers",
        "author": "Nicholas C. Zakas",
        "published": "2016",
        "publisher": "No Starch Press",
        "language": "English",
        "admeasurement":"15,88 × 23,50 cm",
        "weight":"235,068 g",
        "pages": 352
    },
    {
        "id":6,
        "isbn": "9781491904244",
        "title": "You Don't Know JS",
        "subtitle": "ES6 & Beyond",
        "author": "Kyle Simpson",
        "published": "2015",
        "publisher": "O'Reilly Media",
        "language": "English",
        "admeasurement":"15,88 × 23,50 cm",
        "weight":"235,068 g",
        "pages": 278
    },
    {
        "id":7,
        "isbn": "9781449325862",
        "title": "Git Pocket Guide",
        "subtitle": "A Working Introduction",
        "author": "Richard E. Silverman",
        "published": "2013",
        "publisher": "O'Reilly Media",
        "language": "English",
        "admeasurement":"15,88 × 23,50 cm",
        "weight":"235,068 g",
        "pages": 234
    },
    {
        "id":8,
        "isbn": "9781449337711",
        "title": "Designing Evolvable Web APIs with ASP.NET",
        "subtitle": "Harnessing the Power of the Web",
        "author": "Glenn Block, et al.",
        "published": "2014",
        "publisher": "O'Reilly Media",
        "language": "English",
        "admeasurement":"15,88 × 23,50 cm",
        "weight":"235,068 g",
        "pages": 538
    }
    ];
  }
  
  export default booksReducer;
  